
// ===============================================================================================================
// -*- C++ -*-
//
// Array.inl - Dynamic array template class.
//
// Copyright (c) 2010 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef ARRAY_HPP
#define ARRAY_HPP

// =====================================================
// Template Class Array
// A STL vector-like class with some extra functionality
// =====================================================

template<class T>
class Array
{
public:

	typedef size_t size_type;
	typedef T value_type;

	typedef T& reference;
	typedef const T& const_reference;

	typedef T* pointer;
	typedef const T* const_pointer;

	Array();
	Array(size_type count);
	Array(size_type count, const value_type & val);

	void resize(size_type count);
	void push_back(const value_type & val);
	void pop_back();
	int  add_unique(const value_type & val);
	int  contains(const value_type & val) const;
	void fill(const value_type & val);
	void remove(const value_type & val);
	void erase(size_type index);
	void clear();

	~Array();

	size_type size() const
	{
		return array_size;
	}

	bool empty() const
	{
		return array_size == 0;
	}

	reference at(size_type index)
	{
		return data_ptr[index];
	}

	const_reference at(size_type index) const
	{
		return data_ptr[index];
	}

	reference operator[] (size_type index)
	{
		return data_ptr[index];
	}

	const_reference operator[] (size_type index) const
	{
		return data_ptr[index];
	}

	pointer data()
	{
		return data_ptr;
	}

	const_pointer data() const
	{
		return data_ptr;
	}

private:

	value_type * data_ptr;
	size_type ptr_size;
	size_type array_size;

	// Elements per allocation (Powers of 2):
	enum { ALLOC_SIZE = ( (sizeof(T) <= 1) ? 32 \
	                    : (sizeof(T) <= 2) ? 16 \
	                    : (sizeof(T) <= 4) ? 8  \
	                    : (sizeof(T) <= 8) ? 4 : 2 ) };
};

// == Class Array ===============================================================================================================

template<class T>
Array<T>::Array()
	: data_ptr(0), ptr_size(0), array_size(0)
{
}

template<class T>
Array<T>::Array(size_type count)
	: data_ptr(0), ptr_size(0), array_size(0)
{
	resize(count);
}

template<class T>
Array<T>::Array(size_type count, const value_type & val)
	: data_ptr(0), ptr_size(0), array_size(0)
{
	resize(count);
	fill(val);
}

template<class T>
void Array<T>::resize(size_type count)
{
	if ((count > ptr_size) && (count > 0))
	{
		value_type * old = data_ptr;

		ptr_size = count;
		data_ptr = new T [ptr_size];

		if (old != 0)
		{
			for (size_type i = 0; i < array_size; i++)
			{
				data_ptr[i] = old[i];
			}
			delete[] old;
		}

		array_size = count;
	}
}

template<class T>
void Array<T>::push_back(const value_type & val)
{
	if (array_size == ptr_size)
	{
		size_type old_size = array_size;
		resize(ptr_size + ALLOC_SIZE);
		array_size = old_size;
	}

	data_ptr[array_size++] = val;
}

template<class T>
void Array<T>::pop_back()
{
	if (array_size > 0)
	{
		array_size--;
	}
}

template<class T>
int Array<T>::add_unique(const value_type & val)
{
	int res = contains(val);
	if (res == 0)
	{
		push_back(val);
	}
	return res;
}

template<class T>
int Array<T>::contains(const value_type & val) const
{
	int count = 0;
	for (size_type i = 0; i < array_size; i++)
	{
		if (data_ptr[i] == val)
		{
			count++;
		}
	}
	return count;
}

template<class T>
void Array<T>::fill(const value_type & val)
{
	for (size_type i = 0; i < array_size; i++)
	{
		data_ptr[i] = val;
	}
}

template<class T>
void Array<T>::remove(const value_type & val)
{
	const value_type element = val;
	for (size_type i = 0; i < array_size; i++)
	{
		if (data_ptr[i] == element)
		{
			erase(i);
		}
	}
}

template<class T>
void Array<T>::erase(size_type index)
{
	if ((array_size > 0) && (index < array_size))
	{
		array_size--;
		while (index < array_size)
		{
			data_ptr[index] = data_ptr[index+1];
			index++;
		}
	}
}

template<class T>
void Array<T>::clear()
{
	if (data_ptr)
	{
		delete[] data_ptr;
		data_ptr = 0;

		ptr_size   = 0;
		array_size = 0;
	}
}

template<class T>
Array<T>::~Array()
{
	if (data_ptr)
	{
		delete[] data_ptr;
	}
}

#endif // ARRAY_HPP
