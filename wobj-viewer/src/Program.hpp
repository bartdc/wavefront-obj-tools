
// ===============================================================================================================
// -*- C++ -*-
//
// Program.hpp - Program entry point.
//
// Copyright (c) 2010 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include "Vector.hpp"
#include "Utility.hpp"
#include "Texture.hpp"
#include "WavefrontObject.hpp"
#include "Mesh.hpp"

// =========================
// Program Class (Singleton)
// =========================

class Program
{
public:

	// Start the program. Called by main().
	static void Initialize(int window_width, int window_height, int argc, char * argv[]);

	// Run the program. Called by main().
	static void Execute();

	// Cleanup and exit. Called by atexit()/exit().
	static void Shutdown();

private:

	 Program() { }
	~Program() { }

	static void Render();
	static void ResizeWindow(int window_width, int window_height);
	static void KeyboardCallback(unsigned char key, int x, int y);
	static void MouseCallback(int button, int state, int x, int y);
	static void MouseMotionCallback(int x, int y);
	static void MenuCallback(int value);
	static void LoadObject(const std::string & filename);
	static void SetUpMenus(const Mesh * pUserMesh);
	static int  GetClickedGroup(int x, int y);

	struct UserObjectInfo
	{
		Mesh * pMesh;

		unsigned int nVertexCount;
		unsigned int nNormalCount;
		unsigned int nUVCount;

		int nDrawByGroupID;
		int nSelectedGroup;
		float fLoadTimeInSecs;
	};

	static int m_MenuIds[2];
	static UserObjectInfo m_LoadedObj;
	static std::vector<std::string> m_vMeshGroups;

	static int m_WinId;
	static int m_window_width;
	static int m_window_height;

	static Vec2 m_LastMousePos;
	static Vec2 m_CurrentMousePos;
	static bool m_MouseMov;

	static float m_AngleX;
	static float m_AngleY;
	static float m_Zoom;

	static bool m_Wireframe;
	static bool m_ShowText;
};

#endif // PROGRAM_HPP
