
## Wavefront OBJ viewer and PMesh Optimizer

Wavefront Object (`.obj`) model loading and manipulation libraries and tools
that I developed during my degree in Digital Games Development.

- `wobj-viewer/` directory contains a simple Wavefront OBJ model loader and viewer, written in C++ and
based on fixed-function OpenGL. It compiles under Linux, MacOS and Windows. A makefile is provided (requires GLUT).
Code is fairly well documented and hopefully easy to follow. I have cleaned it up several times
over the years, but nevertheless, it is quite outdated and not very relevant anymore regarding the
rendering techniques, due to its use of legacy OpenGL.

- `progressive-mesh/` directory contains the same OBJ model loader and viewer
plus an implementation of the *Progressive Mesh* polygon reduction algorithm presented
by Stan Melax in his [website](http://www.melax.com/polychop) and [GDMag article](http://www.melax.com/gdmag.pdf?attredirects=0).

----

A couple screenshots:

![obj-viewer](https://bytebucket.org/glampert/wavefront-obj-tools/raw/a887d56b57e2e944e2b6f39f840f78000822be81/misc/obj-viewer-4.png "obj-viewer")

![obj-viewer](https://bytebucket.org/glampert/wavefront-obj-tools/raw/a887d56b57e2e944e2b6f39f840f78000822be81/misc/obj-viewer-5.png "obj-viewer")

![progressive-mesh](https://bytebucket.org/glampert/wavefront-obj-tools/raw/a887d56b57e2e944e2b6f39f840f78000822be81/misc/obj-viewer-pmesh-1.png "progressive-mesh")

![progressive-mesh](https://bytebucket.org/glampert/wavefront-obj-tools/raw/a887d56b57e2e944e2b6f39f840f78000822be81/misc/obj-viewer-pmesh-2.png "progressive-mesh")

----

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

